from conans import ConanFile

class HelloConan(ConanFile):
    name = "TCLAP"
    version = "master"
    license = "https://github.com/eile/tclap/blob/master/COPYING"
    author = "Michael E. Smoot"
    # We can just omit the settings attribute too
    settings = None

    def build(self):
         #empty too, nothing to build in header only
         pass

    def source(self):
        self.run("wget https://sourceforge.net/projects/tclap/files/tclap-1.2.2.tar.gz/download -O tclap-1.2.2.tar.gz && tar zxf tclap-1.2.2.tar.gz")

    def package(self):
        self.copy("*.h", dst="include", src="tclap-1.2.2/include/")

