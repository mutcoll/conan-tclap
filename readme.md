
# Notes to users

to use this package add the next line to your conanfile.txt:
```
TCLAP/master@jmmut/testing
```
and then the regular process for conan:
```
conan remote add jmmut-commonlibs https://api.bintray.com/conan/jmmut/commonlibs
conan install .. --build missing
```

# Notes to me

## To update the bintray package

- authenticate with "conan user"
- update recipe (conanfile.py)
- commit and push
- conan export . TCLAP/master@jmmut/testing
- conan upload TCLAP/master@jmmut/testing



